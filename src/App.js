import React, {Component} from 'react';
import { Switch, Route } from 'react-router-dom';
import './App.css';
import ViewAdmin from './admin/index';
import View from './view/index';

class App extends Component{
  
    render() {
     
        return (
            <Switch>
                <Route path="/" exact component={View} />
                <Route path="/ViewAdmin" component={ViewAdmin} isPrivate/>
                {/* redirect user to SignIn page if route does not exist and user is not authenticated */}
                <Route component={View} />
            </Switch>
        );
    };
}

export default App;
