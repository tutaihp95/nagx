import React, {Component} from 'react';
import {connect} from 'react-redux';
import * as actions from './../../actions/index';

class TaskForm extends Component{

    constructor(props){
        super(props);
        this.state=({
            id:'',
            name:'',
            fileIMG:'',
            status:false,
        })
    }

    UNSAFE_componentWillMount(){
        if(this.props.itemEditing && this.props.itemEditing.id !==null){
            this.setState({
                id : this.props.itemEditing.id,
                name : this.props.itemEditing.name,
                fileIMG : this.props.itemEditing.fileIMG,
                status : this.props.itemEditing.status,
            });
        } else{
            this.onReset();
        }
    }

    UNSAFE_componentWillReceiveProps(nextProps){
        if(nextProps && nextProps.itemEditing){
            this.setState({
                id : nextProps.itemEditing.id,
                name : nextProps.itemEditing.name,
                fileIMG : nextProps.itemEditing.fileIMG,
                status : nextProps.itemEditing.status,
            });
        } else{
            this.onReset();
        }
    }
    CloseForm=()=>{
        this.props.onCloseForm();
    }

    onChange=(event)=>{
        var target = event.target;
        var name = target.name;
        var value = target.value;
        if(name==="fileIMG"){
            // value = target.value.slice(11);
            console.log(value);
        }
        this.setState({
            [name] : value,
        });

    }

    onSave=(event)=>{
        event.preventDefault();
        this.props.onSaveTask(this.state);
        this.onReset();
        this.CloseForm();
    }

    onReset=()=>{
        this.setState({
            name:'',
            fileIMG:'',
            status:false
        })
    }

    render() {
        if( !this.props.isDisplayForm ) return null;
        return (
            <div className="card text-left">
            <div className="card-header">
            { this.state.id ?'Thêm Ảnh':'Cập nhật Ảnh'}
                <span className="fas fa-times-circle" onClick={this.CloseForm}/>
            </div>
            <div className="card-body">
                <form onSubmit={this.onSave}>
                    <div className="form-group">
                        <label>Tên :</label>
                        <input 
                            type="text" 
                            className="form-control" 
                            name="name" 
                            value={this.state.name}
                            onChange={this.onChange}
                        />
                    </div>
                    <div className="form-group">
                        <label>UP ảnh :</label>
                        <input 
                            type="file" 
                            className="form-control" 
                            name="fileIMG" 
                            // value={this.state.fileIMG}
                            onChange={this.onChange}
                        />
                    </div>
                    <div className="form-group">
                        <label>Trạng thái :</label>
                        <select 
                            name="status" 
                            className="form-control"
                            value={this.state.status}
                            onChange={this.onChange}
                        >
                            <option value={true}>Kích hoạt</option>
                            <option value={false}>Ẩn</option>
                        </select>
                    </div>
                    <div className="text-center">
                        <button className="btn btn-warning" type="submit">
                            <span className="fas fa-plus mr-2" /> Lưu lại
                        </button>
                        <button 
                            className="btn btn-danger" 
                            type="button"
                            onClick={this.onReset}
                        >
                            <span className="fas fa-times mr-2" /> Hủy bỏ
                        </button>
                    </div>
                </form>
            </div>
        </div>
            );
    };
}

const mapStateToProps = state =>{
    return {
        isDisplayForm : state.isDisplayForm,
        itemEditing: state.itemEditing,
    }
};

const mapDispatchToProps = (dispatch, props) =>{
    return {
        onSaveTask : (task) =>{
            dispatch(actions.saveTask(task));
        },
        onCloseForm : ()=>{
            dispatch(actions.closeForm())
        },
    }
}

export default connect(mapStateToProps,mapDispatchToProps)(TaskForm);
