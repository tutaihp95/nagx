import React, {Component} from 'react';
import { connect } from 'react-redux';
import * as actions from './../../actions/index';

class TaskItem extends Component{

    showStatusElsment(){
        return (
            <button className= { this.props.task.status?'alert alert-danger':'alert alert-success'}
                onClick={this.onUpdateStatus}
                >
                    {this.props.task.status?'Kích hoạt':'Ẩn'}
                </button>
        )
    }
    onEditTask=()=>{
        this.props.onOpenForm(this.props.task.id);
        this.props.onEditTask(this.props.task);
    }

    onUpdateStatus=()=>{
        this.props.onUpdateStatus(this.props.task.id);
    }

    onDeleteItem=()=>{
        this.props.onDeleteTask(this.props.task.id);
        // dispatch(actions.deleteItem)
        this.props.onCloseForm();
    }
    render() {
        var {task,index} = this.props;
        return (
            <tr>
            <td> {index+1} </td>
            <td>{task.name}</td>
            <td>
                <img src={`/img${task.fileIMG.slice(11)}`} alt="" style={{maxWidth: 100}}/>
            </td>
            <td className="text-center">
                { this.showStatusElsment() }
            </td>
            <td className="text-center">
                <button 
                    type="button" 
                    className="btn btn-warning"
                    onClick={this.onEditTask}
                >
                    <span className="fas fa-edit mr-2" 
                    />Sửa
                </button>
                <button 
                    type="button" 
                    className="btn btn-danger"
                    onClick={this.onDeleteItem}
                >
                    <span className="fas fa-trash mr-2" />Xóa
                </button>
            </td>
        </tr>
            );
    };
}

const mapStateToProps = state =>{
    return {
    };
}

const mapDispatchToProps = (dispatch,props)=>{
    return {
        onUpdateStatus: (id) =>{
            dispatch(actions.updateStatus(id));
        },
        
        onDeleteTask : (id) =>{
            dispatch(actions.deleteTask(id))
        },
      
        onCloseForm : ()=>{
            dispatch(actions.closeForm())
        },

        onOpenForm : ()=>{
            dispatch(actions.openForm())
        },

        onEditTask : (task)=>{
            dispatch(actions.editTask(task))
        }
    };
}
export default connect(mapStateToProps,mapDispatchToProps)(TaskItem);

