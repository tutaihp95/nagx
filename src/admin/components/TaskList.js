import React, {Component} from 'react';
import TaskItem from './TaskItem';
import { connect } from 'react-redux';
import * as actions from './../../actions/index';

class TaskList extends Component{

    constructor(props){
        super(props);
        this.state= {
            filterStatus:-1,
            filterName:''
        };
    }
    EditFormList=(id)=>{
        this.props.EditFormApp(id);
    }

    onChange=(event)=>{
        var target = event.target;
        var name = target.name;
        var value = target.value;
        var filter = {
            name : name === 'filterName' ? value : this.state.filterName,
            status : name === 'filterStatus' ? value : this.state.filterStatus
        };
        this.props.onFilterTable(filter);
        this.setState({
            [name]:value
        }); 
    }
    render() {
        var { tasks,filterTable, keyword, sort } = this.props;

          if(filterTable.name){
            tasks=tasks.filter((task) =>{
                return task.name.toLowerCase().indexOf(filterTable.name.toLowerCase()) !==-1;
            });
        }
        tasks=tasks.filter((task) => {
            if(filterTable.status===-1){
                return task;
            }else{
                return task.status===(filterTable.status === 1 ? true : false);
            }
        }); 
        // if(keyword){
            tasks=tasks.filter((task) =>{
                return task.name.toLowerCase().indexOf(keyword.toLowerCase()) !==-1;
            });
        // }
        if(sort.by === 'name'){
                    tasks.sort((a,b) =>{
                        if(a.name > b.name) return sort.value;
                        else if(a.name < b.name) return -sort.value;
                        else return 0;
                    });
                }else{
                    tasks.sort((a,b) =>{
                        if(a.status > b.status) return -sort.value;
                        else if(a.status < b.status) return sort.value;
                        else return 0;
                    });
                }
        var { filterName, filterStatus } = this.state;
        var elmTasks = tasks.map((task, index) =>{
            return <TaskItem 
                        key={index}
                        index = {index}
                        task={task}
                    />
        })

      

        return (
            <div className="row mt-3">
            <div className="col-xl-12 col-sm-12 col-md-12 col-lg-12">
                <table className="table table-bordered table-hover">
                    <thead>
                        <tr>
                            <th >STT</th>
                            <th>Tên</th>
                            <th>Ảnh up</th>
                            <th>Trạng thái</th>
                            <th>Hành động</th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <td />
                            <td>
                                <input 
                                    type="text" 
                                    className="form-control" 
                                    name="filterName" 
                                    value={filterName}
                                    onChange={this.onChange}
                                />
                            </td>
                            <td>
                                
                            </td>
                            <td>
                                <select 
                                    name="filterStatus" 
                                    className="form-control"
                                    value={filterStatus}
                                    onChange={this.onChange}
                                >
                                    <option value={-1}>Tất cả</option>
                                    <option value={0}>Ẩn</option>
                                    <option value={1}>Kích hoạt</option>
                                </select>
                            </td>
                            <td />
                        </tr>
                        {elmTasks}
                    </tbody>
                </table>
            </div>
        </div>
        );
    }; 
}

const mapStateToProps = (state) =>{
    return {
        tasks : state.tasks,
        filterTable : state.filterTable,
        keyword : state.search,
        sort : state.sort
    } 
}

const mapDispatchToProps = (dispatch,props)=>{
    return {
       onFilterTable: (filter) =>{
           dispatch(actions.filterTask(filter));
       }

    };
}
export default connect(mapStateToProps,mapDispatchToProps)(TaskList);
