import React, {Component} from 'react';
import { connect } from 'react-redux';
import * as actions from './../../actions/index';

class TaskSearch extends Component{

    constructor(props){
        super(props);
        this.state= {
            keyword: ''
        };
    }
    onChange=(event)=>{
        var target = event.target;
        var keyword = target.value;
        // if(keyword===''){
        //     this.setState({
        //         keyword:''
        //     });
        //     this.props.onSearchControl(this.state.keyword==='');
        // }else{
        //      this.setState({
        //     keyword:keyword
        // });
        // }
        this.setState({
            keyword:keyword
        });
    }
    onSearch=()=>{
        this.props.onSearch(this.state.keyword);
    }
    render() {
        return (
            <div className="col-xl-6 col-sm-6 col-md-6 col-lg-6">
            <div className="input-group">
                <input 
                    type="text" 
                    name="keyword" 
                    className="form-control" 
                    placeholder="Nhập từ khóa..." 
                    onChange={this.onChange}
                    value={this.state.keyword}
                />
                <span className="input-group-btn">
                    <button 
                        type="button" 
                        className="btn btn-primary"
                        onClick= {this.onSearch}
                    >
                        <span className="fas fa-search" /> Tìm
                </button>
                </span>
            </div>
        </div>
        );
    };
}

const mapStateToProps = state =>{
    return {
       
    };
}

const mapDispatchToProps = (dispatch,props)=>{
    return {
        onSearch : (keyword) =>{
            dispatch(actions.searchTask(keyword));
        },
    };
}
export default connect(mapStateToProps,mapDispatchToProps)(TaskSearch);
