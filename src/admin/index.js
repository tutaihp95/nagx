import React, {Component} from 'react';
// import './App.css';
import TaskControl from './components/TaskControl';
import TaskList from './components/TaskList';
import TaskForm from './components/TaskForm';
import { connect } from 'react-redux';
import * as actions from './../actions/index';

class ViewAdmin extends Component{

    onToggleForm=()=>{
        var { itemEditing } = this.props;
        if(itemEditing && itemEditing.id !== ''){
            this.props.onOpenForm();
        }
        else{
            this.props.onToggleForm();
        }
        
        this.props.onClearTask({
            id : '',
            name : '',
            status : false
        });
    }

    render() {
        var { isDisplayForm } = this.props;
        return (
            <div className="container d-block">
                <a className="btn btn-success position-absolute" style={{right:0}} href="/">
                    <span className="button-label">View</span>
                </a>
                <div className="text-center">
                    <h1 > Quản lý danh sách </h1>
                </div>

                <div className="row">
                    <div className={isDisplayForm===true?'col-xl-4 col-sm-4 col-md-4 col-lg-4':''} >
                        {/* form */}
                        <TaskForm />
                    </div>
                    <div className={isDisplayForm===true?'col-xl-8 col-sm-8 col-md-8 col-lg-8':'col-xl-12 col-sm-12 col-md-12 col-lg-12'}>
                        <button 
                            type="button" 
                            className="btn btn-primary"
                            onClick={this.onToggleForm}
                        >
                            <span className="fas fa-plus mr-2" /> Thêm Ảnh
                        </button>

                        {/* search - sort */}
                        <TaskControl />
                        {/* list */}
                         <TaskList />
                    </div>
                </div>

            </div>
        );
    };
}

const mapStateToProps = state =>{
    return {
        isDisplayForm : state.isDisplayForm,
        itemEditing : state.itemEditing,
    };
}

const mapDispatchToProps = (dispatch,props)=>{
    return {
        onToggleForm: () =>{
            dispatch(actions.toggleForm())
        },
      
        onOpenForm : ()=>{
            dispatch(actions.openForm())
        },
        onClearTask : (task) =>{
            dispatch(actions.editTask(task))
        },

    };
}
export default connect(mapStateToProps,mapDispatchToProps)(ViewAdmin);
