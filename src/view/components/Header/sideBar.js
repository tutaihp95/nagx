import React, {Component} from 'react';

class SideBar extends Component{
 
    render() {
        return (
            <div className="controls-container">
                <div className="controls-background" data-js-background>
                    <canvas width={960} height={960} className="controls-header-image" />
                </div>
                <div className="controls-left">
                    <div className="t-logo-container">
                    <a className="t-logo" href="https://www.tumblr.com">
                        <svg className="svg-logo logo-t" width="20px" height="33px" viewBox="0 0 21 37" xmlns="http://www.w3.org/2000/svg">
                        <g className="logotype" fill="#000000" strokeWidth={1} fillRule="evenodd">
                            <path d="M21 37h-6C9 37 5 34 5 27V16H0v-6C6 9 8 4 8 0h6v9h7v7h-7v10c0 3 1 4 4 4h3z" fillRule="evenodd" />
                        </g>
                        </svg>
                        <svg className="svg-logo logo-tumblr" width="245px" height="50px" viewBox="0 0 245 50" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlnsXlink="http://www.w3.org/1999/xlink">
                        <g className="logotype" stroke="none" strokeWidth={1} fill="#ffffff" fillRule="evenodd">
                            <path d="M28.087 49.57h-8.286c-7.461 0-13.021-3.835-13.021-13.01V21.863H0v-7.957C7.461 11.97 10.582 5.556 10.94 0h7.749v12.616h9.04v9.248h-9.04v12.795c0 3.836 1.937 5.162 5.022 5.162h4.376v9.749zm143.951.43c-3.766 0-7.246-1.254-10.151-3.692v3.262h-11.372V7.993h-4.95V0h17.075v15.305c2.224-1.72 5.919-3.119 9.613-3.119 10.51 0 15.676 7.312 15.676 18.46 0 11.612-5.56 19.354-15.89 19.354zm18.115-.43v-7.921h4.197V7.993h-4.95V0h16.93v41.649h4.377v7.92h-20.554zm-112.456 0v-7.921h4.197V20.68h-4.95v-8.065H92.26V17.6c2.582-3.549 7.39-5.413 11.765-5.413 5.525 0 9.22 2.223 11.658 5.914 1.938-3.261 6.529-5.914 12.125-5.914 11.335 0 14.205 8.961 14.205 16.56v12.903h4.125v7.92h-20.16v-7.92h4.054V27.348c0-3.047-1.04-5.628-5.309-5.628-5.273 0-6.816 4.122-6.816 6.13v13.799h4.233v7.92h-20.267v-7.92H106V27.348c0-3.047-.86-5.628-5.093-5.628-5.238 0-6.96 4.122-6.96 6.13v13.799h4.162v7.92H77.697zm137.494 0v-7.921h4.09V20.68h-4.449v-8.065h15.39v5.341c1.9-3.548 6.133-5.77 10.725-5.77H245V22.76h-5.022c-3.766 0-8.717 1.541-8.717 8.566V41.65h4.126v7.92H215.19zM47.53 50c-11.765 0-14.348-8.853-14.348-14.552V12.616h12.411v22.33c0 3.083.754 5.52 4.807 5.52 6.313 0 7.246-4.695 7.246-5.807V20.681h-4.807v-8.065h16.86V41.65h3.55v7.92H59.296V44.91C56.641 48.172 51.762 50 47.529 50zm121.926-8.996c1.902-.215 6.278-.538 6.278-10.287 0-6.774-2.978-9.498-6.35-9.498-2.905 0-7.102 1.54-7.102 10.25 0 7.24 3.551 9.535 7.174 9.535z" fillRule="evenodd" />
                        </g>
                        </svg>
                    </a>
                    </div>
                </div>
                <div className="controls-center">
                    <div className="search-field-container non-essential" data-js-search-container>
                        <input className="search-field-input" type="text" placeholder="Chercher sur agirlmagazine"  autoComplete="off" data-js-search />
                        <i className="fas fa-search"></i>
                    </div>
                </div>
                <div className="controls-right">
                    <div className="buttons-container" data-js-buttons> 
                        <a className="tx-icon-button dashboard-button non-essential" role="button" href="/" data-js-dashboard aria-label="Tableau de bord" title="Tableau de bord">
                            <i className="fas fa-home"></i>
                        </a> 
                        <a className="tx-icon-button message-button" role="button" href="/" data-js-message aria-label="Message" title="Message">
                            <i className="fas fa-comment-medical"></i>
                        </a>
                    <div className="buttons-container non-essential hidden" data-js-snowman-buttons>
                        <button className="tx-button tx-button--tertiary unfollow-button " data-js-unfollow aria-label="Se désabonner" aria-pressed="true">
                            <span className="button-label">Se désabonner</span>
                        </button>
                        <button className="tx-button tx-button--tertiary block-button " data-js-block aria-label="Bloquer">
                            <span className="button-label">Bloquer</span>
                        </button>
                    </div>
                    <button className="tx-icon-button snowman-button non-essential" data-js-snowman aria-label="Blogs">
                        <i className="fas fa-user"></i>
                        <span className="subscription-indicator " data-js-subscription-indicator /> 
                        <span className="button-label">Blogs</span>
                    </button>
                    <a className="tx-button subscribe-button " href="/ViewAdmin">
                        <span className="button-label">ViewQL</span>
                    </a>

                    <button className="tx-button unsubscribe-button hidden" data-js-unsubscribe aria-label="Arrêter les notifications" aria-pressed="true">
                        <span className="button-label">Arrêter les notifications</span>
                    </button>
                    <button className="tx-button follow-button hidden" data-js-follow aria-label="S'abonner" aria-pressed="false">
                        <span className="button-label">S'abonner</span>
                    </button>
                    </div>
                </div>
                </div>

            
        );
    };
}

export default SideBar;
