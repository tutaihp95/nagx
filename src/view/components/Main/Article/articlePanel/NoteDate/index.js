import React, {Component} from 'react';
import NoteDetail from './noteDetail';

class NoteDate extends Component{

    onIsDisplayNote=()=>{
        this.props.onIsDisplayNoteDate();
        this.setState({
            isDisplayNote: this.props.onIsDisplayNoteDate,
        })
    }
    render() {
        var { listNote }= this.props;
        if(listNote && this.props.onIsDisplayNoteDate){
             var elmListNote = listNote.map((list,index) => {
            return  <NoteDetail
                        key={index}
                        index = {index}
                        list={list}
                    />;
        });
        }
       
        return (
        <section className="inline-meta date-notes">
            <span className="notes-pop-container">
                <section className="notes-wrapper clearfix show active">
                <div id="notes" className="notes-anchor" />
                    <ol className="notes">{/* START NOTES */}
                        {elmListNote}
                    </ol>
                </section>
            </span>
        </section>

        );
    };
}

export default NoteDate;
