import React, {Component} from 'react';

class NoteDetail extends Component{

    render() {
        var {list} = this.props;
        return (
            <li className="note like tumblelog_namminhanh without_commentary">
                <a rel="nofollow" className="avatar_frame" target="_blank"  href="/" title="Untitled ">
                    <img src={ list.avaNote } className="avatar " alt={ list.nameNote } />
                </a>
                <span className="action">
                    <a rel="nofollow" href="/" title="Untitled">{ list.nameNote }</a> { list.Note}
                </span>
                <div className="clear" />
            </li>
        );
    };
}

export default NoteDetail;
