import React, {Component} from 'react';
import NoteDate from './NoteDate/index';
import PostControl from './postControl/index';

class ArticlePanel extends Component{

    constructor(props){
        super(props);
        this.state={
            isDisplayNote: false,
            listNote:[
                {
                    idNote:'',
                    avaNote:'https://assets.tumblr.com/images/default_avatar/cone_closed_64.png',
                    nameNote:'namminhanh',
                    Note:'liked this',
                },
                {
                    idNote:'',
                    avaNote:'https://assets.tumblr.com/images/default_avatar/pyramid_closed_64.png',
                    nameNote:'keensludgecolorkid',
                    Note:'liked this',
                },
            ],
        };
    }

    onIsDisplayNote=()=>{
        this.setState({
            isDisplayNote: !this.state.isDisplayNote,
        });
    }
    render() {
        var {listNote} = this.state;
        return (
            <section className="panel">
                <footer className="post-footer">

                    <PostControl listNote={listNote} onIsDisplayNote={this.onIsDisplayNote}/>

                    <NoteDate listNote={listNote} onIsDisplayNoteDate={this.state.isDisplayNote}/>
                </footer>
            </section>

        );
    };
}

export default ArticlePanel;
