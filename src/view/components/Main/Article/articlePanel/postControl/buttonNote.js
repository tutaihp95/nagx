import React, {Component} from 'react';

class ButtonNote extends Component{

    onIsDisplayNote=(e)=>{
        e.preventDefault();
        this.props.onIsDisplayNote();
    }
    render() {
        var {listNote} = this.props;
        
        return (
            <section className="inline-meta date-notes">
                <div className="date-note-wrapper">
                <span>
                    <a href="/" 
                    onClick={this.onIsDisplayNote} 
                    className="meta-item post-notes"
                    >
                        {listNote ? listNote.length : '0'} notes
                    </a>
                </span>
                </div>
            </section>
        );
    };
}

export default ButtonNote;
