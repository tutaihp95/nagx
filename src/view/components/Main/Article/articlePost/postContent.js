import React, {Component} from 'react';

class PostContent extends Component{

    render() {
        var {articlesPost} = this.props;
        return (
            <figure className="post-content high-res with-caption">
                <div className="photo-wrapper">
                <div className="photo-wrapper-inner">
                    <a href="/">
                        <img src={`/img${articlesPost.fileIMG.slice(11)}`} alt={articlesPost.name} width={1280} height={852} />
                    </a>
                </div>
                </div>
                <figcaption className="caption">
                    <p>{articlesPost.name}</p>
                </figcaption>
            </figure>
        );
    };
}

export default PostContent;
