import React, {Component} from 'react';
import ArticlePanel from './articlePanel/index';
import ArticlePost from './articlePost/index';

class Article extends Component{
    
    render() {
        var {article} = this.props;
        if(article.status===false){
            return '';
        }
        return (
                <article className="photo not-page ">
                    <div className="post-wrapper clearfix">
                        <header className="post-header">
                        </header>

                        <ArticlePost article={article}/>

                        <ArticlePanel article={article}/>

                    </div>
                </article>
        );
    };
}

export default Article;
